package EmpresaVehiculo.Vehiculos;

public class VehiculoNoExisteException extends RuntimeException{
    public VehiculoNoExisteException(){
        super(" Error. El vehiculo no existe en la base de datos");
    }
}
