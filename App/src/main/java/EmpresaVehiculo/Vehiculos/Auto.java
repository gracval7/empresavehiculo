package EmpresaVehiculo.Vehiculos;

public class Auto extends Vehiculo implements Alquiler {

    private String situacionactual;


    public Auto (String marca,String patente,Boolean tieneaireacond, Boolean tienelevantacristaleselectricos,Boolean tienealarma,Integer kilometraje, String situacionactual){
    super(marca,patente,tieneaireacond,tienelevantacristaleselectricos,tienealarma, kilometraje);
    this.situacionactual=situacionactual;
    }
    //get
    public String getSituacionactual() {
        return situacionactual;
    }

    public String getPatente(){
      return this.patente;
    }
    
    //set
    public void setSituacionactual(String situacionactual) {
        this.situacionactual = situacionactual;
    }

     //para consultar si el vehiculo esta disponible para alquiler o vender
     @Override
     public void disponibilidadVehiculo(boolean estaocupado){
     if (estaocupado){
        this.situacionactual= "Ocupado";
     }else{
        this.situacionactual="Libre";
     }

     }
    
     //listar vehiculo
     public void listarVehiculo(){
        System.out.println(" Marca:"+this.marca+" patente "+this.patente);
        System.out.println(" Precio venta: "+calcularPrecioVenta());
        System.out.println(" Precio alquiler por plaza y por dia "+calcularPrecioAlquiler());
        System.out.println(" Disponibilidad: "+this.situacionactual);
     }
 
     //calcular precio venta
     public double calcularPrecioVenta(){
        double precioVenta=0;
        double porcentaje= 0;
        if(this.tieneaireacond) porcentaje += 0.02;
        
        if(this.tienealarma) porcentaje += 0.01;

        if(this.tienelevantacristaleselectricos) porcentaje += 0.05;

        porcentaje += 0.35;

        precioVenta= (PRECIO_BASE_VENTA* porcentaje)+ PRECIO_BASE_VENTA;
        return precioVenta;

     }

     @Override
     public double calcularPrecioAlquiler(){
        double precioAlquiler=0;

        precioAlquiler= PRECIO_BASE_ALQUILER_XDIA+ 50;

        return precioAlquiler;
     }
    
}
