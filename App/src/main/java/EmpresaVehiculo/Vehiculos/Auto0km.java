package EmpresaVehiculo.Vehiculos;

public  class Auto0km extends Vehiculo {

    private Boolean estaenparque;
    private String situacionactual;
 
    public Auto0km(String marca,String patente,Boolean tieneaireacond, Boolean tienelevantacristaleselectricos,Boolean tienealarma, Boolean estaenparque, String situacionactual){    
    super(marca,patente,tieneaireacond,tienelevantacristaleselectricos,tienealarma);
    this.estaenparque=estaenparque;
    this.situacionactual=situacionactual;

    }

    //get
    public Boolean getEstaenparque() {
        return estaenparque;
    }
    public String getSituacionactual() {
        return situacionactual;
    }
    public String getPatente(){
    return patente;
    }

    //set
    public void setEstaenparque(Boolean estaenparque) {
        this.estaenparque = estaenparque;
    }
    public void setSituacionactual(String situacionactual) {
        this.situacionactual = situacionactual;
    }

    @Override
     public void disponibilidadVehiculo(boolean estaocupado){
     if (estaocupado){
        this.situacionactual= "Ocupado";
     }else{
        this.situacionactual="Libre";
     }

     }
    
     //listar vehiculo
     public void listarVehiculo(){
        System.out.println(" Marca:"+this.marca+" patente "+this.patente);
        System.out.println(" Precio venta: "+calcularPrecioVenta());
        System.out.println(" Disponibilidad: "+this.situacionactual);
     }
 
     //calcular precio venta
     public double calcularPrecioVenta(){
        double precioVenta=0;
        double porcentaje= 0;
        if(this.tieneaireacond) porcentaje += 0.02;
        
        if(this.tienealarma) porcentaje += 0.01;

        if(this.tienelevantacristaleselectricos) porcentaje += 0.05;

        porcentaje += 0.50;

        precioVenta= (PRECIO_BASE_VENTA* porcentaje)+ PRECIO_BASE_VENTA;
        return precioVenta;

     }
    
}
