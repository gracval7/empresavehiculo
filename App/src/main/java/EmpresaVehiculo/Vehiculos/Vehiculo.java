package EmpresaVehiculo.Vehiculos;

public abstract class Vehiculo {
    public static final Double PRECIO_BASE_VENTA = 5000.00;
    protected String marca;
    protected String patente;
    protected Boolean tieneaireacond;
    protected Boolean tienelevantacristaleselectricos;
    protected Boolean tienealarma;
    protected Integer kilometraje;

    public Vehiculo(String marca,String patente,Boolean tieneaireacond, Boolean tienelevantacristaleselectricos,Boolean tienealarma,Integer kilometraje){
        this.marca=marca;
        this.patente=patente;
        this.tieneaireacond=tieneaireacond;
        this.tienelevantacristaleselectricos=tienelevantacristaleselectricos;
        this.tienealarma=tienealarma;
        this.kilometraje=kilometraje;
    }    
    public Vehiculo(String marca,String patente,Boolean tieneaireacond, Boolean tienelevantacristaleselectricos,Boolean tienealarma){
        this.marca=marca;
        this.patente=patente;
        this.tieneaireacond=tieneaireacond;
        this.tienelevantacristaleselectricos=tienelevantacristaleselectricos;
        this.tienealarma=tienealarma;
    }    


    //para consultar si el vehiculo esta disponible para alquiler o vender
    public abstract void disponibilidadVehiculo(boolean estado);
    
    //listar vehiculo
    public abstract void listarVehiculo();

    //calcular precio venta
    public abstract double calcularPrecioVenta();
    
    public String getPatente() {
        return patente;
    }
    public String getMarca() {
        return marca;
    }

    
}
