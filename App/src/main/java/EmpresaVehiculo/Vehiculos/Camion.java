package EmpresaVehiculo.Vehiculos;

public class Camion extends Vehiculo implements Alquiler {

    private String estadoactual;
   
    public Camion(String marca,String patente,Boolean tieneaireacond,Boolean tienelevantacristaleselectricos,Boolean tienealarma, Integer kilometraje, String estadoactual){
        super(marca,patente,tieneaireacond,tienelevantacristaleselectricos,tienealarma, kilometraje);
        this.estadoactual=estadoactual;

    }

     //get
     public String getEstadoactual() {
         return estadoactual;
     }
     public String getPatente(){
      return patente;
     }
     
     //set
     public void setEstadoactual(String estadoactual) {
         this.estadoactual = estadoactual;
     }

    @Override
     public void disponibilidadVehiculo(boolean estaocupado){
     if (estaocupado){
        this.estadoactual= "Ocupado";
     }else{
        this.estadoactual="Libre";
     }

     }
    
     //listar vehiculo
     public void listarVehiculo(){
        System.out.println(" Marca:"+this.marca+" patente "+this.patente);
        System.out.println(" Disponibilidad: "+this.estadoactual);
        System.out.println(" Precio venta: "+calcularPrecioVenta());
        System.out.println(" Precio alquiler por plaza y por dia "+calcularPrecioAlquiler());
     }
 
     //calcular precio venta
     public double calcularPrecioVenta(){
        double precioVenta=0;
        double porcentaje= 0;
        if(this.tieneaireacond) porcentaje += 0.02;
        
        if(this.tienealarma) porcentaje += 0.01;

        if(this.tienelevantacristaleselectricos) porcentaje += 0.05;
 
        porcentaje += 0.35;
        precioVenta= (PRECIO_BASE_VENTA* porcentaje)+ PRECIO_BASE_VENTA;
        return precioVenta;

     }

     @Override
     public double calcularPrecioAlquiler(){
        double precioAlquiler=0;

        precioAlquiler= PRECIO_BASE_ALQUILER_XDIA+ 300;

        return precioAlquiler;
        

     }
    
}
