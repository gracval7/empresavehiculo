package EmpresaVehiculo.Vehiculos;

public interface Alquiler {

    public static final double PRECIO_BASE_ALQUILER_XDIA = 1000.00;

    public double calcularPrecioAlquiler();

}
