package EmpresaVehiculo.Vehiculos;

public class VehiculoExistenteException extends RuntimeException{
    public VehiculoExistenteException(){
        super("Error. El vehiculo existe en la Base de Datos");
    }
}
