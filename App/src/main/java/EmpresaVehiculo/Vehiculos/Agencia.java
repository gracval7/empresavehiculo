package EmpresaVehiculo.Vehiculos;

import java.util.ArrayList;

public class Agencia {

     private ArrayList <Vehiculo> vehiculos= new ArrayList<Vehiculo>();
 
     //constructor

     public Agencia (){
     
      setVehiculos(new ArrayList<Vehiculo>());
   }

   //get
   public ArrayList<Vehiculo> getVehiculos() {
      return vehiculos;
   }

   //set
   public void setVehiculos(ArrayList<Vehiculo> vehiculos) {
      this.vehiculos = vehiculos;
   }

   //muestro los datos de los vehiculos existentes
   public void listarVehiculos(){

      for(Vehiculo vehiculo : vehiculos){  //recorro el array list de los vehiculos
         
         vehiculo.listarVehiculo();   //muestro los datos de cada vehiculo aplicando el conceptos de polimorfismo
         System.out.println("___________________________________________________");
      }
   }


   // Ingresa vehiculo nuevo
   public void addVehiculo(Vehiculo vehiculonuevo)throws VehiculoExistenteException{

      Vehiculo vehiencontrado= buscarVehiculo(vehiculonuevo.getPatente()); //buscamos el vehiculo por el numero de patente
      
      if (vehiencontrado== null){   //no existe el vehiculo
         this.vehiculos.add(vehiculonuevo);  //lo agregamos al array list
      }else{
         throw new VehiculoExistenteException();  //no existe. Lanzamos la exception
      }
      
   }
 

   //Elimina vehiculo
   public void eliminaVehiculo(String patente)throws VehiculoNoExisteException{

      Vehiculo vehiculoencontrado=buscarVehiculo(patente); //buscamos el vehiculo

      if(vehiculoencontrado==null){
         throw new VehiculoNoExisteException();  //si no existe el vehiculo mandamos la exception
      }else this.vehiculos.remove(vehiculoencontrado);   //caso contrario, eliminamos el vehiculo
   }


   //busca el vehiculo por la patente
   public Vehiculo buscarVehiculo(String patente){  

      Vehiculo vehiculocomprobar= null;  

      for(Vehiculo vehiculo : vehiculos){   //recorremos el Array List de vehiculos
         if(vehiculo.getPatente().compareToIgnoreCase(patente)==0){  //comprobamos si existe el vehiculo
             vehiculocomprobar= vehiculo;   // si existe, guardamos la informacion del vehiculo
             break;
          }
      }
          return vehiculocomprobar;  // retorna el resultado de la busqueda
   }


   //busca vehiculo si no lo encuentra lanza una exception
   public Vehiculo getVehiculo(String patente){

      Vehiculo vehiculoencontrado=buscarVehiculo(patente);  //buscamos el vehiculo por la patente

      if(vehiculoencontrado!= null){   //si existe el vehiculo
         return  vehiculoencontrado;  //retornamos el vehiculo encontrado
      } else throw new VehiculoNoExisteException();  //si no existe el vehiculo, lanzamos una exception
   }
}
