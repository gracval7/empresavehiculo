package EmpresaVehiculo.Vehiculos;
public interface Venta {
    public double calcularPrecioVenta();
    public void mostrarPrecioVenta();
}
