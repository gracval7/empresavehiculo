package EmpresaVehiculo;

import EmpresaVehiculo.Reservas.Cliente;
import EmpresaVehiculo.Reservas.FicheroReserva;
import EmpresaVehiculo.Reservas.Localidad;
import EmpresaVehiculo.Reservas.Reserva;
import EmpresaVehiculo.Reservas.Sucursal;
import EmpresaVehiculo.Vehiculos.Agencia;
import EmpresaVehiculo.Vehiculos.Auto;
import EmpresaVehiculo.Vehiculos.Auto0km;
import EmpresaVehiculo.Vehiculos.Camioneta;
import EmpresaVehiculo.Vehiculos.Minibus;

import java.time.LocalDate;

public class Main{
public static void main( String args[]){

    //gestion de vehiculos

Auto autousadouno = new Auto ("Ford","A1006",true, false,true,9000,"libre");

Camioneta camionetauno = new Camioneta("Toyota","800SA",false, true, true, 20000,"ocupado");

Auto0km autoeventa1 = new Auto0km("Ford", "A985H1", true,true,true,true,"libre");

Minibus minibusuno= new Minibus("Hyundai","N03P8", true,false,true,1500,"libre");

Agencia agencia1 = new Agencia();

agencia1.addVehiculo(autousadouno);
agencia1.addVehiculo(minibusuno);
agencia1.addVehiculo(autoeventa1);
agencia1.addVehiculo(camionetauno);


//gestion de reservas

//creo fichero de las reserva que va a gestionar las reservas

FicheroReserva carpeta1 = new FicheroReserva();

//creo las variables que va a contener la reserva1

Localidad localidadreserva = new Localidad("San Fernando del Valle ","k4700");
Localidad localidadentrega = new Localidad("Buenos Aires", "14008");
Sucursal sucursalreserva = new Sucursal(localidadreserva,"AgenciaCatam", "Mate de Luna 500");
Sucursal sucursalentrega = new Sucursal(localidadentrega,"AgenciaCordob","Av. America 4400");
Cliente clienteuno = new Cliente("Juan Jose Cordoba","jj3453@gmail.com","1756789", 32,"Mastercard");

//creo la reserva1
Reserva reserva1 = new Reserva(agencia1.getVehiculo("N03P8"), sucursalentrega,sucursalreserva,LocalDate.of(2024,02,12),4,302,clienteuno,true,false,false);

// agrego la reserva1 a la carpeta
carpeta1.addReserva(reserva1);

//creo una segunda reserva
Localidad localreserva = new Localidad("San Miguel de Tucuman ","T4000");
Localidad localentrega = new Localidad("Buenos Aires", "C1059ABT");
Sucursal sucursalreserv = new Sucursal(localreserva,"AgenciaTucuman", "Av. Aconquija 850");
Sucursal sucursalentreg = new Sucursal(localentrega,"AgenciaBuenosA.","Av. Santa Fe 1234");
Cliente clientedos = new Cliente("Fernando Perez","FPerezz@gmail.com","1730789", 33,"American Express");

Reserva reserva2 = new Reserva(agencia1.getVehiculo("A1006"), sucursalentreg,sucursalreserv,LocalDate.of(2024,04,01),7,305,clientedos,true,true,false);

//agrega la segunda reserva al fichero de reservas
carpeta1.addReserva(reserva2);

// muestro los datos de las reservas existentes
carpeta1.listarReservas(); 

//listamos los vehiculos existentes
agencia1.listarVehiculos();

}

}