package EmpresaVehiculo.Reservas;

public class ReservaNoEncontradaException extends RuntimeException {
    public ReservaNoEncontradaException(){
        super("La reserva no se encuentra en la base de datos");
    }
}
