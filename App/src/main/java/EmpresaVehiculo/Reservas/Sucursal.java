package EmpresaVehiculo.Reservas;

public class Sucursal {

    private Localidad localidadsucursal;
    private String nombre;
    private String direccion;

    public Sucursal(Localidad localidadsucursal, String nombre, String direccion){
        this.localidadsucursal= localidadsucursal;
        this.nombre=nombre;
        this.direccion=direccion;
    }

    //gets
    public Localidad getLocalidadsucursal() {
        return localidadsucursal;
    }
    public String getNombre() {
        return nombre;
    }
    public String getDireccion() {
        return direccion;
    }

    @Override
    public boolean equals(Object obj) {
        
        return super.equals(obj);
    }
}
