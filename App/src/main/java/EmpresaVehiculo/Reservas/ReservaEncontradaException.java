package EmpresaVehiculo.Reservas;

public class ReservaEncontradaException extends RuntimeException{
    public ReservaEncontradaException (){
        super("La reserva ya existe en la base de datos");
    }
}
