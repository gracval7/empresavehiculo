package EmpresaVehiculo.Reservas;

public class Localidad {
    
    private String nombre;
    private String codigopostal;

    public Localidad(String nombre, String codigopstal){
    this.nombre=nombre;
    this.codigopostal=codigopstal;
    
    }

    //gets
    public String getCodigopostal() {
        return codigopostal;
    }
    public String getNombre() {
        return nombre;
    }
    
}
