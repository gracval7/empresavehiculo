package EmpresaVehiculo.Reservas;
import java.util.ArrayList;

public class FicheroReserva {

    private ArrayList<Reserva> reservas = new ArrayList<>();

    public FicheroReserva(){
        setReservas(new ArrayList<Reserva> ());
    }
    //set
    public void setReservas(ArrayList<Reserva> reservas) {
        this.reservas = reservas;
    }

    //agregar reserva
    public void addReserva(Reserva reservanueva){

        Reserva reservaexistente = null;
        reservaexistente=buscarReserva(reservanueva.getNumreserva());

        if(reservaexistente== null){  //no existe la reserva
            this.reservas.add(reservanueva);  //ingresamos la reserva a la base de datos
            reservanueva.getVehiculoreservado().disponibilidadVehiculo(true); //modificamos la disponibilidad del vehiculo, ahora esta ocupado
        }else throw new ReservaEncontradaException();
    

    }

    //elimino reserva
    public void eliminarReserva(Integer numreserva)throws ReservaNoEncontradaException{
        Reserva reservaeliminar= buscarReserva(numreserva);
        if (reservaeliminar!=null){
        this.reservas.remove(reservaeliminar);
        }else{
            throw new ReservaNoEncontradaException();
        }
    }

    //buscar reserva
    public Reserva getReserva(Integer numreserva)throws ReservaNoEncontradaException{
     Reserva reservabuscar = buscarReserva(numreserva);
     if(reservabuscar==null){

       throw new ReservaNoEncontradaException();
     }else return reservabuscar;
    }

    //comprueba si existe reserva
    public Reserva buscarReserva(Integer numreserva){
        Reserva reservaencontrada=null;
        for(Reserva reserva: reservas){
           
            if(reserva.getNumreserva()==numreserva){
                reservaencontrada=reserva;
                break;
            }
        }
        return reservaencontrada;
    }

    //listar reserva
    public void listarReservas(){
        for(Reserva reserva : reservas){

            System.out.println("     RESERVA       ");
            System.out.println(" numero: " +reserva.getNumreserva()+" fecha reserva:  "+reserva.getFechainicio());
            System.out.println(" fecha de entrega: "+reserva.calcularFechaEntregaReserva());
            System.out.println(" dias de reserva: "+reserva.getDiasreserva());
            System.out.println(" Vehiculo  modelo "+reserva.getVehiculoreservado().getMarca()+" patente: "+reserva.getVehiculoreservado().getPatente());
            System.out.println(" Precio reserva: "+reserva.precioReserva());
            System.out.println(" en 6 cuotas de: "+reserva.precioCuotas());
            System.out.println("________________________________________________________________");
        }
    }
}
