package EmpresaVehiculo.Reservas;
import java.time.LocalDate;

import EmpresaVehiculo.Vehiculos.Vehiculo;

public class Reserva{
 
    private Integer numreserva;
    private Vehiculo vehiculoreservado;
    private Sucursal entrega;
    private Sucursal  reserva;
    private LocalDate fechainicio;
    private Integer diasreserva;
    private Cliente clientereserva;
    private boolean sillabebe;
    private boolean conductoradicional;
    private boolean unidadgps;

    public Reserva(Vehiculo vehiculoreservado, Sucursal entrega, Sucursal reserva, LocalDate fechaininicio,Integer diasreserva, Integer numreserva, Cliente clientereserva, boolean sillabebe, boolean conductoradicional, boolean unidadgps){
        this.vehiculoreservado=vehiculoreservado;
        this.entrega=entrega;
        this.reserva=reserva;
        this.fechainicio=fechaininicio;
        this.diasreserva=diasreserva;
        this.numreserva=numreserva;
        this.clientereserva=clientereserva;
        this.sillabebe=sillabebe;
        this.conductoradicional=conductoradicional;
        this.unidadgps=unidadgps;
    }
    //gets
    public Sucursal getEntrega() {
        return entrega;
    }
    public Sucursal getReserva() {
        return reserva;
    }
    public Vehiculo getVehiculoreservado() {
        return vehiculoreservado;
    }
    public Integer getDiasreserva() {
        return diasreserva;
    }
    public LocalDate getFechainicio() {
        return fechainicio;
    }
    public Integer getNumreserva() {
        return numreserva;
    }
    public Cliente getClientereserva() {
        return clientereserva;
    }
    public Boolean getSillabebe(){
        return sillabebe;
    }
    public Boolean getConductoradicional(){
        return conductoradicional;
    }

    public Boolean getUnidadgps(){
        return unidadgps;
    }

   //calcula la fecha de entrega del vehiculo
    public LocalDate calcularFechaEntregaReserva(){
        LocalDate fechaentrega;
        
        fechaentrega = this.fechainicio.plusDays(diasreserva);
        return fechaentrega;

    }

    //calcula el precio de la reserva
    public Double precioReserva(){
        double precioReserva = 0.00;
        if(this.getSillabebe()) precioReserva=2500;

        if(this.getUnidadgps()) precioReserva = 3000;
  
        if(this.getConductoradicional()) precioReserva= 2800;

        precioReserva += this.getVehiculoreservado().calcularPrecioVenta();

        if(this.entrega == this.reserva) precioReserva = (precioReserva * 0.35) + precioReserva;
        
        return precioReserva;
    }

    //calcular precio en 6 cuotas

    public double precioCuotas(){
        
        return precioReserva()/6;
    }
    
}
