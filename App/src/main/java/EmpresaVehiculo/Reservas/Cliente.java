package EmpresaVehiculo.Reservas;

public class Cliente {
    
    private String nombre;
    private String correoelectronico ;
    private String documento;
    private Integer edad;
    private String tarjeta;

    public Cliente (String nombre, String correoelectronico, String documento, Integer edad, String tarjeta){
        this.nombre=nombre;
        this.correoelectronico=correoelectronico;
        this.documento=documento;
        this.edad=edad;
        this.tarjeta=tarjeta;
    }

    //get
    public String getCorreoelectronico() {
        return correoelectronico;
    }
    public Integer getEdad() {
        return edad;
    }
    public String getNombre() {
        return nombre;
    }
    public String getDocumento() {
        return documento;
    }
    public String getTarjeta() {
        return tarjeta;
    }

}
